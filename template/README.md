# {{ project_name }}

## Running locally

Install poetry via:

```sh
pipx install poetry && pipx inject poetry poethepoet[poetry_plugin]
```

Install application dependencies via:

```sh
poetry install
```
Start the application via:

```sh
poetry poe up
```

Press Ctrl-C to stop the application.

> **TIP:** You can see all configured `poe` tasks by just running the `poe`
> command with no options.

The application can be stopped via

```sh
poetry poe down
```

Logs can be streamed via:

```sh
docker compose logs -f
```

## Local development

Install git pre-commit hooks via:

```console
poetry run pre-commit install
```

View all the available poe tasks by running:
```console
poe
```

Start the application _from the
production container_:

```console
poetry poe up:production
```

To run pre-commit checks in order to fix up code style and layout to match our
common convention:

```console
poetry poe fix
```

To build or rebuild all container images used by the application:

```console
poetry poe compose:build
```

To pull any images used by the docker compose configuration:

```console
poetry poe compose:pull
```

### Dependencies

> **IMPORTANT:** if you add a new dependency to the application as described
> below you will need to run `docker compose build` or add `--build` to the
> `docker compose run` and/or `docker compose up` command at least once for
> changes to take effect when running code inside containers. The poe tasks have
> already got `--build` appended to the command line.

To add a new dependency _for the application itself_:

```console
poetry add {dependency}
```

To add a new development-time dependency _used only when the application is
running locally in development or in testing_:

```console
poetry add -G dev {dependency}
```

To remove a dependency which is no longer needed:

```console
poetry remove {dependency}
```

## CI configuration

The project is configured with Gitlab AutoDevOps via Gitlab CI using the .gitlab-ci.yml file.
