###############################################################################
# Base image for all *production* images to build upon.
#
# If you change the version here, update pyproject.toml and the version used in
# CI. We explicitly specify the platform here so that it matches the platform
# used in deployment.
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.11-slim AS base

# Some performance and disk-usage optimisations for Python within a docker container.
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

# Set the PORT env variable with a default value
ENV PORT={{ app_port }}

WORKDIR /usr/src/app

# Pretty much everything from here on needs poetry.
RUN pip install --no-cache-dir poetry

###############################################################################
# Just enough to run tox. Tox will install any other dependencies it needs.
# Note that tox runs from the base *production* container so that it uses
# the same platform as will be running in production. This is to provide some
# easy way for developers on Apple Silicon to run tests on the platform used in
# production.
FROM base AS tox

RUN pip install tox
ENTRYPOINT ["tox"]
CMD []

###############################################################################
# Install requirements.
FROM base AS install_deps

COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false && \
    poetry install --no-root --without=dev

###############################################################################
# A development-focussed image. This does not include the actual application
# code since that will be mounted in as a volume.
FROM install_deps AS development

RUN poetry install --only=dev --no-root

CMD exec uvicorn {{ project_slug }}.main:app --reload --host 0.0.0.0 --port $PORT

###############################################################################
# The last target in the file is the "default" one. In our case it is the
# production image.
#
# KEEP THIS AS THE FINAL TARGET OR ELSE DEPLOYMENTS WILL BREAK.
FROM install_deps as production

# The production target includes the application code.
COPY . .

CMD exec uvicorn {{ project_slug }}.main:app --host 0.0.0.0 --port $PORT
