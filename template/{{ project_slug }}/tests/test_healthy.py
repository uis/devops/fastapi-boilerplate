from fastapi.testclient import TestClient

from {{ project_slug }}.main import app

client = TestClient(app)


def test_healthy():
    response = client.get("/healthy")
    assert response.status_code == 200
    assert response.text == "ok"
