from fastapi import FastAPI, Response

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/healthy", response_class=Response)
def read_status():
    return Response(content="ok", status_code=200)
