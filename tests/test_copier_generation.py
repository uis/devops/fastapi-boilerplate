import pytest
import copier
import os
from binaryornot.check import is_binary
import re


PATTERN = b"{{([^{}]+)}}"
RE_OBJ = re.compile(PATTERN)


@pytest.fixture(params=[
    {
        "include_tests": True,
        "include_docker": True,
        "dst": "/tmp/include_both"
    },
    {
        "include_tests": False,
        "include_docker": True,
        "dst": "/tmp/include_docker_only"
    },
    {
        "include_tests": True,
        "include_docker": False,
        "dst": "/tmp/include_tests_only"
    },
    {
        "include_tests": False,
        "include_docker": False,
        "dst": "/tmp/include_none"
    },
])
def context(request):
    """Context for Copier parametrized with common variants."""
    return {
        "project_name": "Test FastAPI Project",
        **request.param
    }


def test_rendered_project(context):
    """Generate project from template using context."""
    generated = copier.Worker(
        src_path=".",
        dst_path=context["dst"],
        defaults=True,
        unsafe=True,
        data=context
    )
    generated.run_copy()
    assert os.path.exists(
        f"{context['dst']}/.copier-answers.yml"
    ), "Copier did not generate the project correctly."


def build_files_list(root_dir):
    """Build a list containing absolute paths to the generated files."""
    return [
        os.path.join(dirpath, filename)
        for dirpath, _, files in os.walk(root_dir)
        for filename in files
    ]


def check_paths(paths):
    """Check all paths to ensure no placeholders are left."""
    for path in paths:
        if is_binary(path):
            continue
        with open(path, 'rb') as file:
            content = file.read()
            match = RE_OBJ.search(content)
            assert match is None, f"Copier variable not replaced in {path}"
