# FastAPI Boilerplate
This repository contains a Copier template for FastAPI projects, maintained by the UIS DevOps Division.

## Quickstart
Note: The copier command will prompt you to answer several questions (command line prompts). Use the defaults if it's your first time, or refer to the Configuration section below for answers to those questions.

First, install `pipx` and required packages:

```bash
$ pipx install copier poetry
$ copier copy --trust --vcs-ref <TAG> https://gitlab.developers.cam.ac.uk/uis/devops/fastapi-boilerplate.git <OUTPUT_PATH>
$ cd <YOUR_PROJECT_DIRECTORY>
$ poetry install
```

Where:
- `<TAG>` is the version of the template, e.g., `1.0.0`.
- `<OUTPUT_PATH>` is the path where the generated content will be stored, which can be relative or absolute.

For further information, please consult the DevOps Division Guidebook entry on "FastAPI Developer Environment".

## Configuration
Copier will request the following values during setup:

- `project_name`: Human-readable name for the project. Default is "FastAPI Application".
- `project_slug`: Filename-safe version of the project name, used as the project directory. Default is auto-generated from the project name by replacing spaces and dashes with underscores.
- `project_description`: Description of the project. Default is "A FastAPI application."
- `author_name`: Name of the project's author. Default is "UIS DevOps Division".
- `author_email`: Email of the author. Default is "devops@uis.cam.ac.uk".
- `api_version`: API version number. Default is "1.0.0".
- `python_version`: Python version to be used. Default is "3.11".
- `include_tests`: Include test setup or not. Default is `true`.
- `include_docker`: Include Docker configuration. Default is `true`.
- `secret_key`: Secret key used in development, generated automatically based on the project slug.

## Testing
The repository setup includes Tox for running automated tests. It checks basic functionality such as project generation and that the included tests run successfully. To execute tests:

```bash
$ tox
```

This will run all tests configured in `tox.ini`, ensuring that both style guidelines and code functionality are maintained.
